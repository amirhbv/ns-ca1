import os
import sys

import scapy.all as scapy


class Host:
    def __init__(self, pcap_file):
        self.packets = scapy.rdpcap(filename=pcap_file)

    def printDnsRequests(self):
        for packet in self.packets:
            # type A queries
            if packet.haslayer(scapy.UDP) and packet[scapy.UDP].sport == 53 and packet.an and packet.an.type == 1:
                print(
                    f'{packet[scapy.DNSQR].qname.decode()[:-1]},{packet.an.rdata}')


class DnsRequestExtractor:
    def __init__(self, pcaps_dirname):

        self.hosts = []
        for pcap_filename in os.listdir(pcaps_dirname):
            self.hosts.append(
                Host(pcaps_dirname + pcap_filename)
            )

    def extract(self):
        print('domain,ip')
        for host in self.hosts:
            host.printDnsRequests()


dnsRequestExtractor = DnsRequestExtractor(sys.argv[1])
dnsRequestExtractor.extract()
