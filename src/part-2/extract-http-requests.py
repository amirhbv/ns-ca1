import os
import sys

import scapy.all as scapy


class Host:
    def __init__(self, pcap_file):
        self.packets = scapy.rdpcap(filename=pcap_file)

    def printHttpRequests(self):
        for session in self.packets.sessions().values():
            for packet in session:
                if packet.haslayer('HTTPRequest'):
                    print(f'{packet[scapy.IP].dst},{packet[scapy.TCP].dport}')


class HttpRequestExtractor:
    def __init__(self, pcaps_dirname):
        scapy.load_layer('http')

        self.hosts = []
        for pcap_filename in os.listdir(pcaps_dirname):
            self.hosts.append(
                Host(pcaps_dirname + pcap_filename)
            )

    def extract(self):
        print('ip,port')
        for host in self.hosts:
            host.printHttpRequests()


httpRequestExtractor = HttpRequestExtractor(sys.argv[1])
httpRequestExtractor.extract()
