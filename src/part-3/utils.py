from cryptography.fernet import Fernet
import socket


def create_tcp_socket(host, port):
    tcp_socket = socket.socket(
        socket.AF_INET,
        socket.SOCK_STREAM,
    )
    tcp_socket.setsockopt(
        socket.SOL_SOCKET,
        socket.SO_REUSEADDR,
        True,
    )
    tcp_socket.bind((host, port))

    return tcp_socket


def generate_key() -> bytes:
    return Fernet.generate_key()


def encrypt_message(message: bytes, key: bytes) -> bytes:
    return Fernet(key).encrypt(message)


def decrypt_message(token: bytes, key: bytes) -> bytes:
    return Fernet(key).decrypt(token)
