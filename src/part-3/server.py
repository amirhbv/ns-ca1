import json
import socket
from threading import Thread
import time

from utils import create_tcp_socket, generate_key, decrypt_message, encrypt_message


class Client:
    def __init__(self, client_id, port):
        self.client_id = client_id
        self.port = port
        self.key = b''
        self.session_keys = {}

    def set_session_key(self, client_id, session_key, send=False):
        self.session_keys[client_id] = session_key
        if send:
            session_socket = socket.socket()
            try:
                session_socket.connect((socket._LOCALHOST, self.port))
            except:
                print('Invalid client port')
                return

            session_socket.sendall(
                encrypt_message(
                    f'{time.time()}:{client_id}:{session_key}'.encode(),
                    key=self.key,
                )
            )


class Server:

    def __init__(self, config):
        self.listen_port = config.get('port', 8000)
        self.clients = {
            client['id']: Client(client['id'], client['port'])
            for client in config.get('clients', [])
        }

        print(self.clients)

    def run(self):
        Thread(target=self._run_command_socket).start()

    def _run_command_socket(self):
        command_socket = create_tcp_socket(
            host=socket._LOCALHOST,
            port=self.listen_port,
        )
        command_socket.listen(5)

        while True:
            client_socket, addr = command_socket.accept()

            handler = ClientHandler(
                client_socket=client_socket,
                server=self,
                addr=addr,
            )

            print(f'new connection from {addr}')
            Thread(target=handler.run).start()

        command_socket.close()


class ClientHandler:

    def __init__(self, client_socket: socket.socket, server: Server, addr):
        self.client_socket = client_socket
        self.server = server
        self.addr = addr

    def run(self):
        while True:
            data = self.client_socket.recv(2048).decode('utf-8')
            print(f'{self.addr} : {data}', end='')
            client_id, message = data.split(' | ')

            try:
                client = self.server.clients.get(client_id)
            except:
                continue

            if message == 'HI':
                session_key = generate_key()
                client.key = session_key
                self.client_socket.sendall(session_key)
            else:
                message = decrypt_message(
                    message.encode(),
                    key=client.key,
                ).decode()
                is_connect_command = 'connect' in message

                if is_connect_command:
                    _, dest_id = message.split(' ')

                    try:
                        dest = self.server.clients.get(dest_id)
                    except:
                        continue

                    session_key = generate_key()
                    self.client_socket.sendall(session_key)
                    client.set_session_key(dest_id, session_key)
                    dest.set_session_key(client_id, session_key, send=True)


with open('./config.json', 'r') as config_file:
    config = json.load(config_file)
    Server(config).run()
