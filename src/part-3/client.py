import socket
import sys
import time
from threading import Thread

from utils import create_tcp_socket, decrypt_message, encrypt_message


class Client:
    BUFFER_SIZE = 2048

    def __init__(self, server_port, client_id, listen_port):
        self.server_port = server_port
        self.client_id = client_id
        self.listen_port = listen_port
        self.key = ''
        self.session_keys = {}

    def _encode_msg(self, msg):
        return f'{self.client_id} | {msg}'.encode('utf-8')

    def run(self):
        Thread(target=self._run_command_socket).start()

        command_socket = socket.socket()
        try:
            command_socket.connect((socket._LOCALHOST, self.server_port))
        except:
            print('Invalid server port')
            return

        command_socket.sendall(self._encode_msg('HI'))
        resp = command_socket.recv(Client.BUFFER_SIZE)
        print(f'key: {resp}')
        self.key = resp

        while True:
            line = input()
            is_connect_command = 'connect' in line

            if is_connect_command:
                message = line
                _, dest_id = line.split(' ')
            else:
                dest_id, message = line.split(' | ')
                message = f'{time.time()}:{dest_id}:{self.session_keys.get(dest_id)}:{message}'

            command_socket.sendall(
                self._encode_msg(
                    encrypt_message(
                        message.encode(),
                        self.key,
                    ).decode()
                )
            )

            if is_connect_command:
                resp = command_socket.recv(Client.BUFFER_SIZE)
                print(f'session key with {dest_id}: {resp}')
                self.session_keys[dest_id] = resp

    def _run_command_socket(self):
        command_socket = create_tcp_socket(
            host=socket._LOCALHOST,
            port=self.listen_port,
        )
        command_socket.listen(5)

        while True:
            client_socket, addr = command_socket.accept()
            data = decrypt_message(
                client_socket.recv(2048),
                self.key,
            )

            print(f'{addr}: {data.decode().split(":")}')

        command_socket.close()


Client(
    server_port=int(sys.argv[1]),
    client_id=sys.argv[2],
    listen_port=int(sys.argv[3]),
).run()
