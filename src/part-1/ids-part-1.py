import os
import sys

import scapy.all as scapy


class Host:
    def __init__(self, name, ip, pcap_file):
        self.name = name
        self.ip = ip
        self.packets = scapy.rdpcap(filename=pcap_file)

    def __str__(self):
        return f'{self.name},{self.ip}'

    def isBlack(self, blacklist):

        for packet in self.packets:
            if packet.haslayer(scapy.IP):
                if packet[scapy.IP].dst in blacklist:
                    return True

            if packet.haslayer(scapy.DNS):
                if packet[scapy.DNSQR].qname.decode()[:-1] in blacklist:
                    return True

        return False


class BlackHostsExtractor:
    def __init__(self, blacklist_filename, pcaps_dirname):

        with open(blacklist_filename, mode='r') as fin:
            self.blacklist = fin.read().splitlines()

        self.hosts = []
        for pcap_filename in os.listdir(pcaps_dirname):
            (name, ip) = pcap_filename.replace('.pcap', '').split('-')
            self.hosts.append(
                Host(name, ip, pcaps_dirname + pcap_filename)
            )

    def extract(self):
        for host in self.hosts:
            if host.isBlack(self.blacklist):
                print(host)


blackHostsExtractor = BlackHostsExtractor(sys.argv[1], sys.argv[2])
blackHostsExtractor.extract()
